class PostModel {
  const PostModel({
    required this.id,
    required this.userId,
    required this.title,
    required this.body,
  });

  final int id;
  final int userId;
  final String title;
  final String body;

  factory PostModel.fromMap(Map<String, dynamic> map) {
    return PostModel(
      id: map['id'].toInt(),
      userId: map['userId'].toInt(),
      title: map['title'],
      body: (map['body'] as String).replaceAll('\n', ' '),
    );
  }
}
