class PostImageModel {
  const PostImageModel({
    required this.id,
    required this.title,
    required this.image,
  });

  final int id;
  final String title;
  final String image;

  factory PostImageModel.fromMap(Map<String, dynamic> map) {
    return PostImageModel(
      id: map['id'] as int,
      title: map['title'] as String,
      image: map['url'] as String,
    );
  }
}
