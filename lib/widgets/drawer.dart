import 'package:flutter/material.dart';

class DrawerWidget extends StatelessWidget {
  const DrawerWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: const [
          UserAccountsDrawerHeader(
            currentAccountPicture: FlutterLogo(),
            accountEmail: Text('teste@mail.com'),
            accountName: Text('Carlos'),
          ),
          ListTile(
            title: Text('Login'),
          ),
          ListTile(
            title: Text('Perfil'),
          ),
          ListTile(
            title: Text('Sair'),
          ),
        ],
      ),
    );
  }
}
