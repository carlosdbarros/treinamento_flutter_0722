import 'package:http/http.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../repositories/repositories.dart';

import 'pages/pages.dart';

class AppModule extends Module {
  @override
  List<Bind> get binds => [
        Bind((_) => Client()),
        Bind<PostRepository>((i) => PostRepository(i())),
      ];

  @override
  List<Module> get imports => [
        PostModule(),
        PostImageModule(),
      ];

  @override
  List<ModularRoute> get routes => [
        ChildRoute(Modular.initialRoute, child: (_, __) => const SplashPage()),
        ChildRoute('/home', child: (_, __) => const HomePage()),
        ModuleRoute('/post', module: PostModule()),
        ModuleRoute('/post-image', module: PostImageModule()),
      ];
}
