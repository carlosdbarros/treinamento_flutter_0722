import 'package:flutter_modular/flutter_modular.dart';

import '../../repositories/repositories.dart';

import 'post.dart';

class PostModule extends Module {
  @override
  List<Bind> get binds => [
        Bind.lazySingleton(
          (i) => PostListController(i.get<PostRepository>()),
          export: true,
        ),
      ];

  @override
  List<ModularRoute> get routes => [
        ChildRoute(
          '/detail',
          child: (context, args) => PostDetailPage(post: args.data),
        ),
      ];
}
