import 'package:flutter/material.dart';

import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../widgets/widgets.dart';

import '../../../models/models.dart';

import 'post_list_controller.dart';

class PostListPage extends StatefulWidget {
  const PostListPage({
    Key? key,
    required this.controller,
  }) : super(key: key);

  final PostListController controller;

  @override
  State<PostListPage> createState() => _PostListPageState();
}

class _PostListPageState extends State<PostListPage> {
  @override
  void initState() {
    super.initState();
    widget.controller.getPosts();
  }

  @override
  Widget build(BuildContext context) {
    final controller = widget.controller;

    return Scaffold(
      drawer: const DrawerWidget(),
      appBar: AppBar(
        title: const Text('Posts'),
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        padding: const EdgeInsets.all(16),
        child: Observer(
          builder: (context) {
            if (controller.isLoading) {
              return const Center(child: CircularProgressIndicator());
            }

            if (controller.mainError?.isNotEmpty == true) {
              return const Center(
                child: Text(
                  'Ops... parece que algo deu errado',
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                ),
              );
            }

            if (controller.posts.isEmpty) {
              return const Center(
                  child: Text(
                'Nenhum post foi encontrado',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
              ));
            }

            return RefreshIndicator(
              onRefresh: controller.getPosts,
              child: ListView.builder(
                physics: const BouncingScrollPhysics(),
                itemCount: controller.posts.length,
                itemBuilder: (context, index) {
                  final post = controller.posts[index];

                  return _PostCard(post: post);
                },
              ),
            );
          },
        ),
      ),
    );
  }
}

class _PostCard extends StatelessWidget {
  const _PostCard({required this.post});

  final PostModel post;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Modular.to.pushNamed('/post/detail', arguments: post),
      child: SizedBox(
        width: double.infinity,
        child: Card(
          margin: const EdgeInsets.only(bottom: 16),
          elevation: 1,
          child: Padding(
            padding: const EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '#${post.id < 10 ? '0' : ''}${post.id} - ${post.title}',
                  style: const TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                  ),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
                const SizedBox(height: 16),
                Text(
                  post.body,
                  style: const TextStyle(fontSize: 16),
                  textAlign: TextAlign.justify,
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
