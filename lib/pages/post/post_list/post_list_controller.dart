// ignore_for_file: library_private_types_in_public_api

import 'package:mobx/mobx.dart';

import '../../../models/models.dart';
import '../../../repositories/repositories.dart';

part 'post_list_controller.g.dart';

class PostListController = _PostListControllerBase with _$PostListController;

abstract class _PostListControllerBase with Store {
  _PostListControllerBase(this.postRepository);

  final PostRepository postRepository;

  @observable
  bool _isLoading = false;
  bool get isLoading => _isLoading;
  @action
  bool _setIsLoading(bool value) => _isLoading = value;

  @observable
  String? _mainError;
  String? get mainError => _mainError;
  @action
  void setMainError(String? value) => _mainError = value;

  @observable
  ObservableList<PostModel> _posts = <PostModel>[].asObservable();
  ObservableList<PostModel> get posts => _posts;
  @action
  ObservableList<PostModel> _setPosts(List<PostModel> posts) =>
      _posts = posts.asObservable();

  Future<void> getPosts() async {
    _setIsLoading(true);

    try {
      final posts = await postRepository.load();
      _setPosts(posts);
    } catch (_) {
      setMainError('Ops... algo deu errado');
    } finally {
      _setIsLoading(false);
    }
  }
}
