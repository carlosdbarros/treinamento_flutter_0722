// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post_list_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$PostListController on _PostListControllerBase, Store {
  late final _$_isLoadingAtom =
      Atom(name: '_PostListControllerBase._isLoading', context: context);

  @override
  bool get _isLoading {
    _$_isLoadingAtom.reportRead();
    return super._isLoading;
  }

  @override
  set _isLoading(bool value) {
    _$_isLoadingAtom.reportWrite(value, super._isLoading, () {
      super._isLoading = value;
    });
  }

  late final _$_mainErrorAtom =
      Atom(name: '_PostListControllerBase._mainError', context: context);

  @override
  String? get _mainError {
    _$_mainErrorAtom.reportRead();
    return super._mainError;
  }

  @override
  set _mainError(String? value) {
    _$_mainErrorAtom.reportWrite(value, super._mainError, () {
      super._mainError = value;
    });
  }

  late final _$_postsAtom =
      Atom(name: '_PostListControllerBase._posts', context: context);

  @override
  ObservableList<PostModel> get _posts {
    _$_postsAtom.reportRead();
    return super._posts;
  }

  @override
  set _posts(ObservableList<PostModel> value) {
    _$_postsAtom.reportWrite(value, super._posts, () {
      super._posts = value;
    });
  }

  late final _$_PostListControllerBaseActionController =
      ActionController(name: '_PostListControllerBase', context: context);

  @override
  bool _setIsLoading(bool value) {
    final _$actionInfo = _$_PostListControllerBaseActionController.startAction(
        name: '_PostListControllerBase._setIsLoading');
    try {
      return super._setIsLoading(value);
    } finally {
      _$_PostListControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setMainError(String? value) {
    final _$actionInfo = _$_PostListControllerBaseActionController.startAction(
        name: '_PostListControllerBase.setMainError');
    try {
      return super.setMainError(value);
    } finally {
      _$_PostListControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  ObservableList<PostModel> _setPosts(List<PostModel> posts) {
    final _$actionInfo = _$_PostListControllerBaseActionController.startAction(
        name: '_PostListControllerBase._setPosts');
    try {
      return super._setPosts(posts);
    } finally {
      _$_PostListControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''

    ''';
  }
}
