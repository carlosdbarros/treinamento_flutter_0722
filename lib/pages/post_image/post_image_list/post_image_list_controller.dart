import 'package:mobx/mobx.dart';

import '../../../models/models.dart';
import '../../../repositories/repositories.dart';

part 'post_image_list_controller.g.dart';

class PostImageListController = _PostImageListControllerBase
    with _$PostImageListController;

abstract class _PostImageListControllerBase with Store {
  _PostImageListControllerBase(this.postRepository);

  final PostRepository postRepository;

  @observable
  bool _isLoading = false;
  bool get isLoading => _isLoading;
  @action
  bool _setIsLoading(bool value) => _isLoading = value;

  @observable
  String? _mainError;
  String? get mainError => _mainError;
  @action
  void setMainError(String? value) => _mainError = value;

  @observable
  ObservableList<PostImageModel> _posts = <PostImageModel>[].asObservable();
  ObservableList<PostImageModel> get posts => _posts;
  @action
  ObservableList<PostImageModel> _setPosts(List<PostImageModel> posts) =>
      _posts = posts.asObservable();

  Future<void> getPosts() async {
    _setIsLoading(true);

    try {
      final posts = await postRepository.loadImages();
      _setPosts(posts);
    } catch (_) {
      setMainError('Ops... algo deu errado');
    } finally {
      _setIsLoading(false);
    }
  }
}
