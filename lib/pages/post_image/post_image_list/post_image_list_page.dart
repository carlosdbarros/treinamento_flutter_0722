import 'package:flutter/material.dart';

import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';

import '../../../widgets/widgets.dart';

import 'post_image_list_controller.dart';

class PostImageListPage extends StatefulWidget {
  const PostImageListPage({super.key, required this.controller});

  final PostImageListController controller;

  @override
  State<PostImageListPage> createState() => _PostImageListPageState();
}

class _PostImageListPageState extends State<PostImageListPage> {
  @override
  void initState() {
    super.initState();
    widget.controller.getPosts();
  }

  @override
  Widget build(BuildContext context) {
    final controller = widget.controller;

    return Scaffold(
      drawer: const DrawerWidget(),
      appBar: AppBar(
        title: const Text('Post com imagem'),
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        padding: const EdgeInsets.all(16),
        child: Observer(builder: (_) {
          if (controller.isLoading) {
            return const Center(child: CircularProgressIndicator());
          }

          if (controller.mainError?.isNotEmpty == true) {
            return const Center(
              child: Text(
                'Ops... parece que algo deu errado',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
              ),
            );
          }

          if (controller.posts.isEmpty) {
            return const Center(
                child: Text(
              'Nenhum post foi encontrado',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
            ));
          }

          return ListView.separated(
            itemCount: controller.posts.length,
            separatorBuilder: (_, __) => const SizedBox(height: 16),
            itemBuilder: (context, index) {
              final post = controller.posts[index];

              return ListTile(
                contentPadding: EdgeInsets.zero,
                leading: Hero(
                  tag: post.title,
                  child: ClipOval(
                    child: Image.network(post.image, width: 50),
                  ),
                ),
                title: Text(post.title),
                onTap: () => Modular.to.pushNamed(
                  '/post-image/detail',
                  arguments: post,
                ),
              );
            },
          );
        }),
      ),
    );
  }
}
