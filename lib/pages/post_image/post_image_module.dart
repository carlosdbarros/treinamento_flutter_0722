import 'package:flutter_modular/flutter_modular.dart';

import 'post_image.dart';

class PostImageModule extends Module {
  @override
  List<Bind> get binds => [
        Bind((i) => PostImageListController(i()), export: true),
      ];

  @override
  List<ModularRoute> get routes => [
        ChildRoute(
          '/detail',
          child: (_, args) => PostImageDetailPage(post: args.data),
        ),
      ];
}
