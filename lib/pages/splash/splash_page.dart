import 'package:flutter/material.dart';

import 'package:flutter_modular/flutter_modular.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({Key? key}) : super(key: key);

  Future<void> _init() async {
    return Future.delayed(const Duration(seconds: 3), () async {
      // atualizar o cache
      print('inicilizando app ...');

      Modular.to.navigate('/home');
    });
  }

  @override
  Widget build(BuildContext context) {
    _init();
    final size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Colors.blue,
      body: SizedBox(
        height: double.infinity,
        width: double.infinity,
        child: Center(
          child: Image.asset('assets/icon.png', width: size.width * .5),
        ),
      ),
    );
  }
}
