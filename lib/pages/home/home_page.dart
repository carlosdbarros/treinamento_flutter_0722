import 'package:flutter/material.dart';

import 'package:flutter_modular/flutter_modular.dart';

import '../../pages/pages.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final PageController controller = PageController();

    return Scaffold(
      body: PageView(
        controller: controller,
        children: [
          PostListPage(controller: Modular.get()),
          PostImageListPage(controller: Modular.get()),
        ],
      ),
    );
  }
}
